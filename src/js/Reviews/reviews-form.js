import {isValid} from './review-utils';

export function reviews() {
	const reviewForm = document.getElementById('review-form');
	reviewForm.addEventListener('submit', submitReviewFormHandler);
}

function submitReviewFormHandler(event) {
	const reviewForm = document.getElementById('review-form');
	const reviewInput = reviewForm.querySelector('#input-review');
	const submitBtnReview = reviewForm.querySelector('#submit-review-btn');
	event.preventDefault();
	if (isValid(reviewInput.value)) {
		const review = {
			text: reviewInput.value.trim(),
			date: new Date().toJSON()
		};
		submitBtnReview.disabled = true;
		console.log(review.text);
		submitBtnReview.disabled = false;
		// Async rq to server
	}

};