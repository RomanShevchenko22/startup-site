import 'bootstrap';
import '../scss/style.scss';
import "firebase/auth";
import {authModal} from './Account/auth';
import {Route} from './Routing/route';
import {Router} from './Routing/router';
import {reviews} from './Reviews/reviews-form';
// import {slideShow} from './Main/slider';

// Init routing
(function () {
	new Router([
		new Route('home', 'home.html', true),
		new Route('reviews', 'reviews.html'),
		new Route('about', 'about.html')
	]);
}());

// Init modal window
(authModal());


const INTERVAL = 500;
window.addEventListener('hashchange', e => {
	if (window.location.hash.substr(1) === 'reviews') {
		let timer = null;

		(function waitLoad() {
			if (document.getElementById('review-form') !== null){
				reviews();
				clearTimeout(timer);
			} else {				
				setTimeout(waitLoad, INTERVAL);
			}
		}());
	}
});



// (slideShow('.slider', {
// 	isAutoplay: true
// }));


const NAV_TOGGLE = document.querySelector(".main-nav__toggle");
const NAV_MENU = document.querySelector(".main-nav__wrapper-list");
NAV_TOGGLE.addEventListener("click", function (evt) {
  evt.preventDefault();
  NAV_TOGGLE.classList.toggle('main-nav__toggle--open');
  NAV_MENU.classList.toggle('main-nav__wrapper-list--open');
});

