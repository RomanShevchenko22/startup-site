// Create modal window
export function createModal(content, modalBody) {
	modalBody.innerHTML = content;	
};

// Notifications
export function chips(message = null, type = null, flag = false, timeremove = 3000) {
	if (flag) {
		document.querySelector('.modal-body').innerHTML = `
		<div class="alert alert-${type} notification-centr">
			<span>
				${message}
			</span>	
		</div>`;
	} else {
		let chips = document.createElement('div');
		let place = document.querySelector('.modal-content');	
		chips.classList.add(`alert`, `alert-${type}`, `fade`, `show`, `notification-top`);	
		chips.innerHTML = `
		<span>
			${message}
		</span>			
		`;
		document.querySelector('.modal-dialog').insertBefore(chips, place);
		setTimeout(() => deleteChip(chips), timeremove);
	};
};
	function deleteChip(chips) {
		chips.remove();
	};

// Error processing
import {
	USER_NOT_FOUND_NOTE,
	WRONG_PASSWORD_NOTE,
	USER_TOO_MANY_RQ_NOTE,
	USER_WEAK_PASSWORD_NOTE,
	EMAIL_IN_USE_NOTE,

	RED_TYPE,
	YELLOW_TYPE
} from './constant'

export function errorProcessing (error) {
	switch (error) {
		case 'auth/user-not-found': chips(USER_NOT_FOUND_NOTE, RED_TYPE);
		break;
		case 'auth/wrong-password': chips(WRONG_PASSWORD_NOTE, RED_TYPE);
		break;
		case 'auth/too-many-requests': chips(USER_TOO_MANY_RQ_NOTE, RED_TYPE);
		break;
		case 'auth/weak-password': chips(USER_WEAK_PASSWORD_NOTE, YELLOW_TYPE);
		break;
		case 'auth/email-already-in-use': chips(EMAIL_IN_USE_NOTE, YELLOW_TYPE);
		break;
		default: console.log(error.code);
	}
}
