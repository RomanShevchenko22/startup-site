import * as firebase from 'firebase/app';
import {
  firebaseConfig,
  PRELOADER,
  MODAL_AUTH_HTML,
	MODAL_BODY,
  MODAL_LABEL,
} from './constant';
import {
  createModal,  
  errorProcessing
} from './utils-auth';
import {registrModal} from './registration';
import {account} from './account';

// Main Auth func
export function authModal () {
  createModal (MODAL_AUTH_HTML, MODAL_BODY);
	MODAL_LABEL.textContent = 'Aвторизация';
	document.getElementById('auth-form').addEventListener('submit', authFormHandler);
	document.getElementById('reg-btn').addEventListener('click', registrModal);
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Add AuthState listener on Firebase
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    account();    
  } else {
    authModal();
  }
  PRELOADER.classList.add('cancel');
});

// Authorization
async function authFormHandler(event) {
  event.preventDefault();
  const button = event.target.querySelector("#enterAuthBtn");
  const email = event.target.querySelector("#email").value;
  const password = event.target.querySelector("#password").value;
  button.disabled = true;
  try {
    await firebase.auth().signInWithEmailAndPassword(email, password)
    .catch(error => errorProcessing(error.code));
  } catch (error) {
    console.log(error);
  }; 
    button.disabled = false;
};
