import * as firebase from 'firebase/app';
import {
  ACCOUNT_HTML,
	MODAL_BODY,
  MODAL_LABEL,
  MODAL_REAUTH,  
  ENTER_BUTTON,
  MODAL_SIGNOUT_SUCCESS,
  GREY_TYPE, 
  MODAL_DELETE_SUCCESS
} from './constant';
import {
  createModal, 
  chips,
  errorProcessing
} from './utils-auth';
import {authModal} from './auth';

export function account() {
  const user = firebase.auth().currentUser;	
  createModal (ACCOUNT_HTML, MODAL_BODY);      
  MODAL_LABEL.textContent = `${user.email}`;
  ENTER_BUTTON.textContent = 'Кабинет';
  document.querySelector('.lead').textContent = `Привет, ${user.email}!`;
  document.getElementById('logoutBtn').addEventListener('click', logOut);
  document.getElementById('delAccBtn').addEventListener('click', deleteAccount);
};

async function logOut() {
  try {
    await firebase.auth().signOut();
    chips(MODAL_SIGNOUT_SUCCESS, GREY_TYPE, true)
    ENTER_BUTTON.textContent = 'Войти';
    setTimeout(authModal, 2500);
  } catch (error) {
    console.log(error);
  }
};

function deleteAccount() {
  createModal (MODAL_REAUTH, MODAL_BODY);
  document.getElementById('back-to-acc').addEventListener('click', account);
  document.getElementById('reauth-form').addEventListener('submit', deleteFormHandler);
};

async function deleteFormHandler(event) {
  event.preventDefault();
  const user = firebase.auth().currentUser;  
  const button = event.target.querySelector("#delAccBtn");
  const password = event.target.querySelector("#password").value;
  button.disabled = true;
  const credential = firebase.auth.EmailAuthProvider.credential(user.email, password);
  try {
    await user.reauthenticateWithCredential(credential)
    .then(function() {
      user
      .delete()
      .then(() =>{
        ENTER_BUTTON.textContent = 'Войти';  
        chips(MODAL_DELETE_SUCCESS, GREY_TYPE, false, 4000);               
      })})
    .catch(error => errorProcessing(error.code));    
  } catch (error) {
    console.log(error);
  };
  button.disabled = false;  
};