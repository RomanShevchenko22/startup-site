import * as firebase from 'firebase/app';
import {
	MODAL_BODY,
	MODAL_REGISTRATION_HTML,
	MODAL_LABEL,	
	MODAL_REGISTR_SUCCESS, 
  GREEN_TYPE		
} from './constant';
import {
	createModal, 
	chips,
	errorProcessing
} from './utils-auth';
import {authModal} from './auth';

// Registration function
export function registrModal() {
	createModal(MODAL_REGISTRATION_HTML, MODAL_BODY);
	MODAL_LABEL.textContent = 'Регистрация';
	document.getElementById('backAuthBtn').addEventListener('click', authModal);
	document.getElementById('reg-form').addEventListener('submit', registrationFormHandler);
};

// Registartion
async function registrationFormHandler(event) {
	event.preventDefault();
	const button = event.target.querySelector("#regBtn");
	const formElements = document.getElementById('reg-form').elements;
	let formData = {};
	for (let i = 0; i < formElements.length; i++) {
		if (formElements[i].type != "submit") formData[formElements[i].name] = formElements[i].value;	
	};	
	button.disabled = true;		
	try {
		await	firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password)
			.then((responce) => {
				if (responce.additionalUserInfo.isNewUser) {
					chips(MODAL_REGISTR_SUCCESS, GREEN_TYPE);
				}
			})
			.catch(error => errorProcessing(error.code));
			button.disabled = false;			
	} catch (error) {
    console.log(error);
  }
};