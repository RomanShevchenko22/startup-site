export const firebaseConfig = {
	apiKey: "AIzaSyAYceidVNg_eRGyni1uJC-vL4lqn8DXqB4",
	authDomain: "startup-project-ccbe9.firebaseapp.com",
	databaseURL: "https://startup-project-ccbe9.firebaseio.com",
	projectId: "startup-project-ccbe9",
	storageBucket: "startup-project-ccbe9.appspot.com",
	messagingSenderId: "955737242143",
	appId: "1:955737242143:web:a82273d8df174ab02bf32a"
};

export const LINKS = document.querySelectorAll('.nav-link');

export const MODAL_BODY = document.querySelector('.modal-body');

export const MODAL_LABEL = document.getElementById('modalLabel');

export const ENTER_BUTTON = document.getElementById('enterBtn');

export const PRELOADER = document.getElementById('preloader')

// Notifications
export const USER_NOT_FOUND_NOTE = 'Пользователь не найден';

export const WRONG_PASSWORD_NOTE = 'Неверный пароль';


export const EMAIL_IN_USE_NOTE = 'Email уже используется';

export const USER_WEAK_PASSWORD_NOTE = 'Пароль должен быть не менее 6 символов';

export const MODAL_REGISTR_SUCCESS = 'Вы успешно зарегистрировались!';


export const MODAL_SIGNOUT_SUCCESS = 'Вы вышли из аккаунта';

export const MODAL_DELETE_SUCCESS = 'Вы удалили аккаунт';


export const USER_TOO_MANY_RQ_NOTE = 'Слишком много запросов. Попробуйте позже';

// Color type Notifications
export const RED_TYPE = 'danger';
export const YELLOW_TYPE = 'warning';
export const GREEN_TYPE = 'success';
export const GREY_TYPE = 'secondary';


export const MODAL_AUTH_HTML = `
<form id="auth-form">
	<div class="form-group">
		<label for="email">Email</label>
		<input type="email" class="form-control" id="email" placeholder="name@example.com" required>
	</div>
	<div class="form-group">
		<label for="password">Пароль</label>
		<input type="password" class="form-control" id="password" required>
	</div>
	<p>Нет логина или пароля? <button type="button" class="btn btn-outline-info btn-outline-info--custom" id="reg-btn">Зарегистрируйтесь</button></p>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
		<button type="submit" class="btn btn-success" id="enterAuthBtn">Войти</button>
	</div>
</form>
`;

export const ACCOUNT_HTML = `
<div class="jumbotron">
<h1 class="display-4">Личный кабинет</h1>
<p class="lead">Привет</p>
<hr class="my-4">
<p>Действия с аккаунтом</p>
<p class="lead">
  <button type="button" class="btn btn-primary" id="logoutBtn">Выйти</button>
	<button type="button" class="btn btn-danger" id="delAccBtn">Удалить</button>
</p>
</div>
`;

export const MODAL_REGISTRATION_HTML = `
<button type="button" class="btn btn-secondary btn-outline-info--custom btn-outline-info--bg" id="backAuthBtn"><span>Назад</span></button>
<form id="reg-form">
	<div class="form-group">
		<label for="name">Имя</label>
		<input name="name" type="text" class="form-control" id="name" required>
	</div>
	<div class="form-group">
		<label for="surname">Фамилия</label>
		<input name="surname" type="text" class="form-control" id="surname" required>
	</div>
	<div class="form-group">
		<label for="reg-email">Email</label>
		<input name="email" type="email" class="form-control" id="reg-email" placeholder="name@example.com" required>
	</div>
	<div class="form-group">
		<label for="reg-password">Пароль</label>
		<input name="password" type="password" class="form-control" id="reg-password" required>
		<small id="passwordHelpInline" class="text-muted">
		Минимум 6 символов
		</small>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
		<button type="submit" class="btn btn-success" id="regBtn">Регистрация</button>
	</div>
</form>
`;
	// <div class="form-group">
	// 	<label for="birthday">Дата рождения</label>
	// 	<input name="birthday" type="date" class="form-control" id="birthday">
	// </div>


export const MODAL_REAUTH = `
<form id="reauth-form">
<div class="form-group">
	<label for="password">Для удаления аккаунта введите пароль ещё раз:</label>
	<input type="password" class="form-control" id="password" required>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal" id="back-to-acc">Отмена</button>
	<button type="submit" class="btn btn-danger" id="delAccBtn">Удалить</button>
</div>
</form>
`;