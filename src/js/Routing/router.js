import {linkChanged} from './utils-route';
import {LINKS} from './constant-route';

export function Router (routes) {
	try {
		if (!routes) {
			throw 'Error: No routes param';
		}
		this.constructor(routes);
		this.init();
	}
	catch (e) {
		console.error(e);
	}
}

Router.prototype = {
	routes: undefined,
	rootElem: undefined,

	// Constructor for array of routes & root element
	constructor: function (routes) {
		this.routes = routes;
		this.rootElem = document.getElementById('app');
	},
	// Init & add EventListener of hashchange
	init: function () {
		let r = this.routes;
		(function (scope, r) {			
			window.addEventListener('hashchange', e => {
				scope.hasChanged(scope, r);
			});
		})(this, r);
		this.hasChanged(this, r);
	},
	// If hash has changed
	hasChanged: function (scope, r) {
		const hash = window.location.hash;	
		if (hash.length > 0) {	
			linkChanged(hash, LINKS);			
			r.forEach(route => {
				if (route.isActiveRoute(hash.substr(1))) {
					scope.goToRoute(route.htmlName);
				}
			});
			// If no routes - default page
		} else {
			linkChanged(0, LINKS);
			r.forEach(route => {
				if (route.default) {
					scope.goToRoute(r[i].htmlName);
				}
			});
		}
	},
	
	// Routing using a new hash, create a GET request
	goToRoute: function (htmlName) {
		(function (scope) {
			let url = './' + htmlName;			
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {
					scope.rootElem.innerHTML = this.responseText;
				}
			};
			xhttp.open('GET', url, true);
			xhttp.send();
		})(this);
	}
}