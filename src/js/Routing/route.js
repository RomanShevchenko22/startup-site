export function Route (name, htmlName, defaultRoute) {
	try {
		if (!name || !htmlName) {
			throw 'Error: no Name or HTMLName params';
		}
		this.constructor(name, htmlName, defaultRoute);
	} catch (e) {
		console.error(e);
	}
}

Route.prototype = {
	name: '',
	htmlName: '',
	default: '',
	//функция-конструктор, присвоение объекту переданных значений
	constructor: function (name, htmlName, defaultRoute) {  
		this.name = name;
		this.htmlName = htmlName;
		this.default = defaultRoute;
	},
	//проверка активности маршрута, получение расположения файла
	isActiveRoute: function (hashedPath) {
		return hashedPath.replace('#', '') === this.name;
	}
}