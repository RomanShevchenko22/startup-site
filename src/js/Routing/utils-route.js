// Routing Bootstrap class Active
export function linkChanged(hash, links) {	
	if (hash != 0) {
		links.forEach(element => {
			if (hash === element.hash) {
				element.classList.add('active');
			}
			else {
				element.classList.remove('active');
			}
		})
	} else {
		links[0].classList.add('active');
	}
};