const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
require("@babel/polyfill");

const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

// Оптимизация одинаковых частей кода в результирующей папке
const optimization = () => {
	const config = {
		splitChunks: {chunks: 'all'}
	}
  if (isProd) {
    config.minimizer = [
      new OptimizeCssAssetsWebpackPlugin(),
      new TerserWebpackPlugin()
    ]
  }
	return config;
}

const filename = ext => isDev ? `[name].${ext}` : `[name].[hash].${ext}`;

const cssLoaders = extra => {
	const loaders = [
		{
			loader: MiniCssExtractPlugin.loader,
			options: {
				hmr: isDev,
				reloadAll: true
			},
		},
		'css-loader'
	]
	if (extra) {
		loaders.push(extra);
	}
	return loaders;
}

module.exports = {
	context: path.resolve(__dirname, 'src'),
	mode: 'development',
	entry: {
		// main: './js/index.js'
		main: ['@babel/polyfill', './js/index.js'],
		
		// Другие точки входа с использованием паттерна [name].
		// filename: [name].bundle.js
	},
	output: {
		filename: filename('js'),
		path: path.resolve(__dirname, 'dist')
	},

  //Элиасы и поддерживаемые автоматически расширения при импорте
	// resolve: {
	// 	extentions: ['.js', '.json'],
	// 	alias: {
	// 		'@models': path.resolve(__dirname, 'src/models'),
	// 		'@': path.resolve(__dirname, 'src'),
	// 	}
	// },

 
	optimization: optimization(),

	devServer: {
		clientLogLevel: 'silent',
		port: 3000,
		hot: true
	},

	plugins: [
		new HTMLWebpackPlugin(
			{
				template: './index.html', 
				minify: {
					collapseWhitespace: isProd
				}
			}
		),
		new HTMLWebpackPlugin(
			{
				filename: 'home.html',
				template: './views/home.html', 
				minify: {
					collapseWhitespace: isProd
				},
				inject: false
			}
		),
		new HTMLWebpackPlugin(
			{
				filename: 'reviews.html',
				template: './views/reviews.html', 
				minify: {
					collapseWhitespace: isProd
				},
				inject: false
			}
		),
		new HTMLWebpackPlugin(
			{
				filename: 'about.html',
				template: './views/about.html', 
				minify: {
					collapseWhitespace: isProd
				},
				inject: false
			}
		),
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
			filename: filename('css')
		}),	
		// Плагин для копирования статики 
		new CopyWebpackPlugin({
			patterns: [
				{
					from: path.resolve(__dirname, 'src/img'),
					to: path.resolve(__dirname, 'dist/img')
			  }
			]
		})
	],

	//Source-Maps
	devtool: (isDev) ? 'source-map' : false,

	module: {
		rules: [
			{
				test: /\.css$/,
				use: cssLoaders()
			}, 
			{
				test: /\.s[ac]ss$/,
				use: cssLoaders('sass-loader')
			}, 
			{
				test: /\.(png|jpg|svg|webp|gif)$/,
				use: ['file-loader']
			},		
			{
				test: /\.(ttf|woff|woff2|eot)$/,
				use: ['file-loader']
			},	
			{
				test: /\.(xml)$/,
				use: ['xml-loader']
			},
			{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: {
					loader: 'babel-loader',
					options: {
						presets: [
							'@babel/preset-env'
						],
						plugins: [
							'@babel/plugin-proposal-class-properties'
						]
					}
				}
      }	
		]
	}
}